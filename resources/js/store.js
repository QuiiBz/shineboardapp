import { getLocalUser } from './helpers/auth';

const user = getLocalUser();

export default {

    state: {

        currentUser:  user,
        isLoggedIn: !!user,
        loading: false,
        auth_error: null,
        cards: [],
    },
    getters: {

        currentUser(state) {

            return state.currentUser;
        },
        isLoggedIn(state) {

            return state.isLoggedIn;
        },
        isLoading(state) {

            return state.loading;
        },
        authError(state) {

            return state.auth_error;
        },
        cards(state) {

            return state.cards;
        }
    },
    mutations: {

        login(state) {

            state.loading = true;
            state.auth_error = null;
        },
        loginSuccess(state, payload) {

            state.auth_error = null;
            state.isLoggedIn = true;
            state.loading = false;

            state.currentUser = Object.assign({}, payload.user, { token: payload.access_token });

            localStorage.setItem('user', JSON.stringify(state.currentUser));
        },
        loginFailed(state, payload) {

            state.loading = false;
            state.auth_error = payload.err;
        },
        logout(state) {

            state.isLoggedIn = false;
            state.currentUser = null;

            localStorage.removeItem('user');
        },
        updateCards(state, payload) {

            state.cards = payload;
        },
        addCard(state, payload) {

            state.cards.push(payload);
        }
    },
    actions: {

        login(context) {

            context.commit('login');
        },
        getCards(context) {

            axios.get('/api/cards', {

                headers: {

                    'Authorization': 'Bearer ' + context.state.currentUser.token,
                }
            }).then((response) => {

                context.commit('updateCards', response.data.cards);
            });
        },
    },
}
