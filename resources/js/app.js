require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';

import { routes } from './routes';

import StoreData from './store';

import MainApp from './components/MainApp';

import { initialize } from './helpers/general';

Vue.use(VueRouter);
Vue.use(Vuex);

const store = new Vuex.Store(StoreData);

const router = new VueRouter({

    routes,
    mode: 'history',
});

initialize(store, router);

Vue.directive('draggable', {

    bind: (element) => {

        let startX, startY, initialMouseX, initialMouseY;

        function mousemove(event) {

            let dx = event.clientX - initialMouseX;
            let dy = event.clientY - initialMouseY;
            element.style.top = startY + dy + 'px';
            element.style.left = startX + dx + 'px';
        }

        function mouseup() {

            document.removeEventListener('mousemove', mousemove);
            document.removeEventListener('mouseup', mouseup);
        }

        element.addEventListener('mousedown', function(event) {

            startX = element.offsetLeft;
            startY = element.offsetTop;
            initialMouseX = event.clientX;
            initialMouseY = event.clientY;
            document.addEventListener('mousemove', mousemove);
            document.addEventListener('mouseup', mouseup);
        });
    }
});

const app = new Vue({

    el: '#app',
    router: router,
    store,
    components: {

        MainApp,
    }
});
