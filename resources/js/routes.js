import Home from './components/pages/Home';
import Header from './components/Header';
import NotFound from './components/pages/errors/NotFound';

import Solutions from './components/pages/Solutions';
import Pricing from './components/pages/Pricing';

import Login from './components/auth/Login';
import Register from './components/auth/Register';

import Dashboard from './components/app/Dashboard';
import DashboardSettings from './components/app/DashboardSettings';

export const routes = [

    {
        path: '/',
        components: { default: Home, header: Header },
    },
    {
        path: '*',
        components: { default: NotFound, header: Header },
    },
    {
        path: '/solutions',
        components: { default: Solutions, header: Header },
    },
    {
        path: '/pricing',
        components: { default: Pricing, header: Header },
    },
    {
        path: '/login',
        components: { default: Login, header: Header },
    },
    {
        path: '/register',
        components: { default: Register, header: Header },
    },
    {
        path: '/dashboard',
        component: Dashboard,
        meta: {

            requiresAuth: true,
        }
    },
    {
        path: '/dashboard/settings',
        component: DashboardSettings,
        meta: {

            requiresAuth: true,
        }
    },
];
