<?php

namespace App\Http\Controllers;

use App\Card;
use Illuminate\Http\Request;

class CardController extends Controller
{
    public function all()
    {
        $cards = Card::all();

        return response()->json([

            'cards' => $cards,
        ], 200);
    }

    public function update()
    {
        $card = Card::where('id', request('id'))->first();

        if($card) {

            $card->update([

                'title' => request('title'),
                'content' => request('content'),
                'x' => substr(request('x'), 0, -2),
                'y' => substr(request('y'), 0, -2),
            ]);

            return response()->json([

                'success' => true,

            ], 200);

        } else {

            Card::create([

                'user' => auth('api')->user()->id,
                'title' => request('title'),
                'content' => request('content'),
                'x' => substr(request('x'), 0, -2),
                'y' => substr(request('y'), 0, -2),
            ]);

            return response()->json([

                'success' => true,

            ], 200);
        }
    }
}
